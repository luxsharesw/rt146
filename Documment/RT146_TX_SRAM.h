/*
 * MA37045_TX_SRAM.h
 *
 *  Created on: 2016/3/1
 *      Author: Ivan_Lin
 */

#ifndef RT146_SRAM_H_
#define RT146_SRAM_H_

#define RT146_TXAD     0x74
#define RT146_CHIPID   0x00

extern void RT146_RESET();
extern void RT146_Init();
extern void T146_Write_All();
extern void T146_Read_All();

extern void SET_BIAS_MOD();
extern unsigned int Get_Bias_Current(unsigned int BIAS_CH);
extern void Bias_MOD_LUT_UpDate_Current( unsigned char CHSEL );


xdata struct RT146_MEMORY
{
	unsigned char CHIPID ;               		  	// reg. address 0x00  R   SRAM Address 0X80
	unsigned char LOL_LOS_STATUS  ;               	// reg. address 0x01  R   SRAM Address 0X81
	unsigned char FAULT_LOL_OR_LOS_STATUS  ;     	// reg. address 0x02  R   SRAM Address 0X82
	unsigned char LOL_LOS_NLAT  ;   			  	// reg. address 0x03  R   SRAM Address 0X83
	unsigned char ADC_OUT  ;     					// reg. address 0x04  R   SRAM Address 0X84
	unsigned char FAULT_NLAT_AGC_RSSI ;        		// reg. address 0x05  R   SRAM Address 0X85
	unsigned char FAULT_STATE ;        				// reg. address 0x06  R   SRAM Address 0X86

	unsigned char SOFT_RESET ;           			// reg. address 0x07  R   SRAM Address 0x87

	unsigned char GLOBLE_INTERFACE_1 ;           	// reg. address 0x08  R   SRAM Address 0x88
	unsigned char MCLKEN_CDR_UNLOCK_THRS ;          // reg. address 0x09  R   SRAM Address 0x89
	unsigned char SW_BIST_TP_CDR_LOOP_BW ;          // reg. address 0x0A  R   SRAM Address 0x8A
	unsigned char DATA_POL ;                		// reg. address 0x0B  RW  SRAM Address 0x8B
	unsigned char CDR_BYP_PWD  ;            		// reg. address 0x0C  RW  SRAM Address 0x8C
	unsigned char LOL_LOS_MASK  ;             		// reg. address 0x0D  RW  SRAM Address 0x8D
	unsigned char LOL_LOS_CLEAR  ;         			// reg. address 0x0E  RW  SRAM Address 0x8E
	unsigned char LOL_LOS_MODE;               		// reg. address 0x0F  RW  SRAM Address 0x8F
	unsigned char FAULT_CLEAR_MASK;              	// reg. address 0x10  RW  SRAM Address 0x90
	unsigned char CH01_LOS_THRS;          			// reg. address 0x11  RW  SRAM Address 0x91
	unsigned char CH23_LOS_THRS;         			// reg. address 0x12  RW  SRAM Address 0x92
	unsigned char FAULT_MODE_LOS_HYST  ; 			// reg. address 0x13  RW  SRAM Address 0x93
	unsigned char CH01_EQ  ;      					// reg. address 0x14  RW  SRAM Address 0x94
	unsigned char CH23_EQ ;      					// reg. address 0x15  RW  SRAM Address 0x95

	unsigned char Reserved_1  ;            			// reg. address 0x16  RW  SRAM Address 0x96

	unsigned char DIS_AUTOMUTE  ;    				// reg. address 0x17  RW  SRAM Address 0x97
	unsigned char CH01_CROSSING_VAL  ;       		// reg. address 0x18  RW  SRAM Address 0x98
	unsigned char CH23_CROSSING_VAL  ;              // reg. address 0x19  RW  SRAM Address 0x99
	unsigned char CH01_PREFALL_VAL  ;              	// reg. address 0x1A  RW  SRAM Address 0x9A
	unsigned char CH23_PREFALL_VAL  ;              	// reg. address 0x1B  RW  SRAM Address 0x9B

	unsigned char DDMI_SETTING_1  ;              	// reg. address 0x1C  RW  SRAM Address 0x9C
	unsigned char DDMI_SETTING_2  ;         		// reg. address 0x1D  RW  SRAM Address 0x9D
	unsigned char DDMI_SETTING_3  ;         		// reg. address 0x1E  RW  SRAM Address 0x9E
	unsigned char DDMI_SETTING_4 ;           		// reg. address 0x1F  RW  SRAM Address 0x9F

	unsigned char POWER_CONTROL_1 ;           		// reg. address 0x20  RW  SRAM Address 0xA0
	unsigned char POWER_CONTROL_2 ;           		// reg. address 0x21  RW  SRAM Address 0xA1

	unsigned char RING_OSC_SETTING ;           		// reg. address 0x22  RW  SRAM Address 0xA2

	unsigned char CH0_IBIAS ;            			// reg. address 0x23  RW  SRAM Address 0xA3
	unsigned char CH1_IBIAS ;            			// reg. address 0x24  RW  SRAM Address 0xA4
	unsigned char CH2_IBIAS ;            			// reg. address 0x25  RW  SRAM Address 0xA5
	unsigned char CH3_IBIAS ;            			// reg. address 0x26  RW  SRAM Address 0xA6
	unsigned char CH0_IMOD ;         				// reg. address 0x27  RW  SRAM Address 0xA7
	unsigned char CH1_IMOD ;         				// reg. address 0x28  RW  SRAM Address 0xA8
	unsigned char CH2_IMOD ;         				// reg. address 0x29  RW  SRAM Address 0xA9
	unsigned char CH3_IMOD ;         				// reg. address 0x2A  RW  SRAM Address 0xAA
	unsigned char CH0_IBUMIN_CONTROL ;              // reg. address 0x2B  RW  SRAM Address 0xAB
	unsigned char CH1_IBUMIN_CONTROL ;      		// reg. address 0x2C  RW  SRAM Address 0xAC
	unsigned char CH2_IBUMIN_CONTROL ;      		// reg. address 0x2D  RW  SRAM Address 0xAD
	unsigned char CH3_IBUMIN_CONTROL ;      		// reg. address 0x2E  RW  SRAM Address 0xAE

	unsigned char RSVD[49];              // 0xAF - 0xF7

	unsigned char Direct_AD;             // Direct_Control Address RW  SRAM Address 0xF8
	unsigned char Direct_Data[5];        // Direct_Control Data    RW  SRAM Address 0xF9 - 0xFD
	unsigned char Direct_RW;             // Direct_Control RW      RW  SRAM Address 0xFE
	unsigned char Direct_EN;             // Direct_Control RW      RW  SRAM Address 0xFF
};

extern xdata struct RT146_MEMORY_MAP;

#define RT146_SIZE   sizeof(RT146_MEMORY_MAP)


#endif /* RT146_SRAM_H_ */
